import Vue from 'vue'
import { Button } from 'element-ui'
import { Form, FormItem} from 'element-ui'  //引用Form, FormItem组件
import { Input} from 'element-ui'  //引用Input组件
import {Message} from 'element-ui'  //引用Message组件(弹框提示)

Vue.use(Button)
Vue.use(Form)  //注册组件
Vue.use(FormItem)  //注册组件
Vue.use(Input)  //注册组件

Vue.prototype.$message = Message  //全局挂载Message组件($message起名随意)

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'
import './assets/css/global.css'  //导入全局样式表
import './assets/fonts/iconfont.css' //导入字体图标

import axios from 'axios'
// axios.defaults.baseURL = 'http://192.168.75.146:8888/api/private/v1'  //配置请求根路径（虚拟机服务）
axios.defaults.baseURL = 'https://lianghj.top:8888/api/private/v1'
Vue.prototype.$http = axios  //挂载axios(每一个组件都可以访问$http,从而发起请求)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
